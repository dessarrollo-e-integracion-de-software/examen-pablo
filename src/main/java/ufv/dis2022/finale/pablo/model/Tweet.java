package ufv.dis2022.finale.pablo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * La clase tweet, es la que contiene toda la información de los tweets que hay guardados
 */
@Data
@AllArgsConstructor
public class Tweet {

    private Integer id;

    private String usuario;

    private String tweet;

    private Date fecha;

    public Tweet() {
        fecha = new Date();
    }
}
