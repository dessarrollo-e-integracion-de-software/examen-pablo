package ufv.dis2022.finale.pablo.views.listtweets;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
import lombok.Data;
import ufv.dis2022.finale.pablo.model.Tweet;
import ufv.dis2022.finale.pablo.views.MainLayout;


@Data
@Route(value = "form", layout = MainLayout.class)
public class TweetForm extends FormLayout {

    //Este atributo me permite ligar los campos del formulario con los campos de la clase Tweet
    Binder<Tweet> binder = new BeanValidationBinder<>(Tweet.class);

    //El tweet que contendrá el formulario
    Tweet tweetInstance;

    //Los dos campos que tendrá el formulario
    TextField usuario = new TextField("Author");
    TextField tweet = new TextField("Message");


    //Botones del formulario
    Button save = new Button("Save");
    Button delete = new Button("Delete");
    Button cancel = new Button("Cancel");

    public TweetForm() {

        addClassName("tweet-form");
        binder.bindInstanceFields(this);

        add(
                usuario,
                tweet,
                createButtonLayout()
        );

    }

    /**
     * Cuando seleccione un tweet de la lista se llamará a este metodo.
     * Lo primero que hago es asignar el tweet al atributo correspondiente.
     * Luego uso el binder.readBean() para que se muestre el contenido del tweet
     * en los campos del formulario
     * @param tweetInstance
     */
    public void setTweetInstance(Tweet tweetInstance){
        this.tweetInstance = tweetInstance;
        binder.readBean(tweetInstance);
    }

    /**
     * Pongo bonitos los botones y les añado los eventos que tienen que realizar.
     * @return
     */
    private Component createButtonLayout() {

        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        //Cuando haga click en el boton de save se llamará al metodo validateAndSave()
        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, tweetInstance)));

        //Cuando haga click en el botón de cancel, se llamarña al evento CloseEvent
        cancel.addClickListener(event -> fireEvent(new CloseEvent(this)));

        //Esto es para poder hacer click con las teclas, en el caso del save me servirá el Enter
        //En el caso de cancel me servirá el escape
        save.addClickShortcut(Key.ENTER);
        cancel.addClickShortcut(Key.ESCAPE);

        //Meto los botones en un layaout horizantal para que salgan uno al lado del otro
        return new HorizontalLayout(save, delete, cancel);
    }


    /**
     * Guarda el tweet en la lista de tweets
     */
    private void validateAndSave() {
        try {
            //Usando el binder guardo en el atributo tweet lo que se ha escrito en el formulario
            binder.writeBean(tweetInstance);
            //Tweets.getInstance().addTweet(tweet);
            //TODO validacion
            //Disparo un evento de tipo guardar para que se guarde el tweet
            fireEvent(new SaveEvent(this, tweetInstance));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Events
    //Clases para controlar los eventos del formulario
    public static abstract class TweetFormEvent extends ComponentEvent<TweetForm> {
        private Tweet tweet;

        protected TweetFormEvent(TweetForm source, Tweet tweet) {
            super(source, false);
            this.tweet = tweet;
        }

        public Tweet getTweet() {
            return tweet;
        }
    }

    public static class SaveEvent extends TweetFormEvent {
        SaveEvent(TweetForm source, Tweet tweet) {
            super(source, tweet);
        }
    }

    public static class DeleteEvent extends TweetFormEvent {
        DeleteEvent(TweetForm source, Tweet tweet) {
            super(source, tweet);
        }
    }

    public static class CloseEvent extends TweetFormEvent {
        CloseEvent(TweetForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
