package ufv.dis2022.finale.pablo.model;

import com.google.gson.GsonBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Clase que contiene la lista de tweets que se van metiendo en la aplicación
 */
public class Tweets {


    //Lista de tweets
    private List<Tweet> tweets = new ArrayList<>();

    //Uso el patrón singleton, de esta forma solo se podrá crear una única instancia de esta clase
    //Para usarla habrá que hacer Tweets.getInstance()
    private static Tweets instance = new Tweets();

    /**
     * Constructora privada para que solo se pueda crear una instancia desde la propia clase
     */
    private Tweets() {
        readTweetsFromFile();
    }

    /**
     * Método estático para el patrón Singleton, devuelve la única instancia de la clase
     * Tweets
     * @return
     */
    public static Tweets getInstance() {
        return instance;
    }

    public void setTweets(List<Tweet> list){
        this.tweets = list;
    }

    /**
     * Añade un tweet a la lista de tweets. Si el tweet no existe, es decir, su ID en null,
     * se le asigna una id y se añade a la lista.
     * Si el tweet ya existe, su id es != null se busca ese tweet, lo borro de la lista,
     * ya que los ArraList no permiten "actualizar", y lo añado de nuevo, con el mismo id
     * @param pTweet - El tweet que se quiere insertar
     * @return Boolean indicando si el proceso se ha realizado correctamente
     */
    public boolean addTweet(Tweet pTweet){
        if(pTweet.getId() == null){
            //El tweet no existe, simplemente lo añado
            pTweet.setId(getNextId());
            tweets.add(pTweet);
        }else{
            //El tweet existe, asi que lo tengo que actualizar

            Tweet tweetToUpdate = getTweetById(pTweet.getId());
            if(tweetToUpdate == null)
                return false;

            tweets.remove(tweetToUpdate);

            tweetToUpdate.setFecha(new Date());
            tweetToUpdate.setUsuario(pTweet.getUsuario());
            tweetToUpdate.setTweet(pTweet.getTweet());

            tweets.add(tweetToUpdate);
        }

        updateJsonFile();
        return true;
    }

    /**
     * Devuelve la siguiente ID a asignar a un tweet, así me aseguro de que no haya
     * ids repetidos.
     * @return
     */
    private Integer getNextId() {

        if(tweets.isEmpty())
            return 1;
        else{

            int mayor = 0;
            for(Tweet t : tweets){
                if(t.getId() > mayor)
                    mayor = t.getId();
            }
            return mayor++;
        }
    }


    /**
     * Dada una id, devuelve el tweet al que corresponde esa ID
     * @param id - La id del tweet
     * @return - El tweet con la misma id, null si no existe ninguno
     */
    private Tweet getTweetById(Integer id){
        return tweets.stream()
                .filter(t -> t.getId().equals(id)) //Filtro en la lista de tweets por aquellos tweets
                                                        //Cuya id correspona con la del parametro id
                .findFirst().orElse(null);        //Devuelvo el primer elemento de la lista.
    }

    /**
     * Vuelca la lista en el fichero json para así llevar un control de la misma
     */
    private void updateJsonFile(){

        String json = new GsonBuilder().setDateFormat("dd-MM-YYYY").create().toJson(tweets.toArray());
        try {
            Files.writeString(Path.of("src/main/resources/twitter.json"), json, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void deleteTweet(Tweet tweet){
        if(tweets.contains(tweet)){
            tweets.remove(tweet);
            updateJsonFile();
        }

    }

    public Boolean readTweetsFromFile(){

        try {
            //Lo convierto a string para poder parsearlo utilizando Gson
            String json = Files.readString(Path.of("src/main/resources/twitter.json"));
            Tweet[] jsonTweets = new GsonBuilder().setDateFormat("dd-MM-YYYY").create().fromJson(json, Tweet[].class);
            if(jsonTweets == null)
                return false;
            Arrays.stream(jsonTweets).forEach(t -> tweets.add(t));
            return true;
        } catch (IOException e) {
            System.err.println("Error reading json file: " + e.getMessage());
            return false;
        }
    }


}
