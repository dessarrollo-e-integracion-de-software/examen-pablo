package ufv.dis2022.finale.pablo.controller;

import com.google.gson.GsonBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ufv.dis2022.finale.pablo.model.Tweet;
import ufv.dis2022.finale.pablo.model.Tweets;

import java.util.List;

@Controller
public class ApiController {



    @PostMapping(path = "/tweet", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> crearTweet(@RequestBody Tweet body){
        Tweets.getInstance().addTweet(body);

        return ResponseEntity.status(HttpStatus.OK).build();
    }


    @GetMapping(path = "/tweet", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> obtenerTweets(){
        List<Tweet> lista = Tweets.getInstance().getTweets();

        String json = new GsonBuilder().setDateFormat("DD-MM-YYYY").create().toJson(lista);
        return ResponseEntity.status(HttpStatus.OK).body(json);
    }

    @DeleteMapping(path = "/tweet", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> eliminarTweet(@RequestBody Tweet tweet){
        Tweets.getInstance().deleteTweet(tweet);

        return ResponseEntity.status(HttpStatus.OK).build();
    }


}
